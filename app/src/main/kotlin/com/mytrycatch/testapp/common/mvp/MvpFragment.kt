package com.mytrycatch.testapp.common.mvp

import android.app.Fragment
import android.content.Context
import org.jetbrains.anko.toast


open class MvpFragment<V : IView, P : IPresenter<V>> : Fragment(), IView {

    protected var presenter: P? = null

    override fun onDestroy() {
        super.onDestroy()
        presenter?.dropView()
    }

    override fun showMessage(message: String) {
        toast(message)
    }

    override fun showMessage(messageResId: Int) {
        toast(messageResId)
    }


    override fun context(): Context {
        return this.activity
    }

}