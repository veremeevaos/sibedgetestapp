package com.mytrycatch.testapp.common.mvp

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.jetbrains.anko.toast

abstract class MvpActivity<V : IView, P : IPresenter<V>> : AppCompatActivity(), IView {

    protected var presenter: P? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.dropView()
    }

    override fun showMessage(message: String) {
        toast(message)
    }

    override fun showMessage(messageResId: Int) {
        toast(messageResId)
    }

    override fun context(): Context {
        return this
    }

}
