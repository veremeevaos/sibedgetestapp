package com.mytrycatch.testapp.common

import android.app.Application
import android.content.Context
import android.preference.PreferenceManager
import android.support.annotation.StringRes
import com.mytrycatch.testapp.Constants
import com.mytrycatch.testapp.R
import java.util.*

object LanguageUtils {

    enum class Language(@StringRes val titleId: Int) {
        RU(R.string.settings_language_item_russian_text),
        EN(R.string.settings_language_item_english_text)
    }

    fun onCreate(context: Context) {
        val lang = getLanguage(context)
        setLocale(context, lang)
    }

    fun getLanguage(context: Context): Language {
        return Language.valueOf(PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(Constants.KEY_LANGUAGE, Language.EN.name))
    }

    fun setLocale(context: Context, language: Language) {
        saveLanguageSettings(context, language)
        updateResources(context, language.name)
    }

    fun updateConfig(app: Application) {
        val lang = getLanguage(app)
        setLocale(app, lang)
    }

    private fun updateResources(context: Context, language: String) {
        val locale = Locale(language)
        Locale.setDefault(locale)

        val resources = context.resources

        val configuration = resources.configuration
        configuration.setLocale(locale)

        resources.updateConfiguration(configuration, resources.displayMetrics)
    }

    private fun saveLanguageSettings(context: Context, language: Language) {
        PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putString(Constants.KEY_LANGUAGE, language.name)
                .commit()
    }

}