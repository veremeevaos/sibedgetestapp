package com.mytrycatch.testapp.common.mvp

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mytrycatch.testapp.R
import kotlinx.android.synthetic.main.layout_recycler_view.*
import java.util.*

abstract class BaseListFragment<M, V : IListView<M>, P : IListPresenter<M, V>> : MvpFragment<V, P>(), IListView<M> {

    protected var dataset: MutableList<M> = ArrayList()
    protected var adapter: RecyclerView.Adapter<*>? = null

    protected var emptyMessageText: String = ""
        set(message) {
            emptyMessageTextView.text = message
        }

    protected var swipeToRefreshEnabled = false
        set (isEnabled) {
            swipeRefreshLayout.isEnabled = isEnabled
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.layout_recycler_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun setLoadingVisible(isVisible: Boolean) {
        if (!swipeRefreshLayout.isRefreshing) {
            progressBar.visibility = if (isVisible) View.VISIBLE else View.GONE
        } else {
            swipeRefreshLayout.isRefreshing = isVisible
        }
    }

    override fun showData(dataset: List<M>) {
        this.dataset.clear()
        this.dataset.addAll(dataset)
        swipeRefreshLayout.isRefreshing = false
        setLoadingVisible(false)
        setEmptyViewVisible(this.dataset.isEmpty())
        adapter?.notifyDataSetChanged()
    }

    override fun updateList() {
        adapter?.notifyDataSetChanged()
        setEmptyViewVisible(this.dataset.isEmpty())
    }

    override fun updateItem(position: Int) {
        adapter?.notifyItemChanged(position)
    }

    override fun removeItem(item: M) {
        dataset.remove(item)
        updateList()
    }

    protected fun onSwipeToRefresh() {
        presenter?.onSwipeToRefresh()
    }

    protected fun setEmptyViewVisible(visible: Boolean) {
        emptyMessageTextView.visibility = if (visible) View.VISIBLE else View.INVISIBLE
    }

    // ===========================================================
    // Methods
    // ===========================================================

    protected open fun initViews() {
        swipeRefreshLayout.isEnabled = swipeToRefreshEnabled
        swipeRefreshLayout!!.setOnRefreshListener {
            onSwipeToRefresh()
        }

        val accentColorId = ContextCompat.getColor(activity, R.color.colorAccent)
        swipeRefreshLayout.setColorSchemeColors(accentColorId)

        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)

        adapter = createAdapter()
        if (adapter == null) {
            throw IllegalArgumentException("createAdapter() should return adapter instance.")
        }
        recyclerView.adapter = adapter
    }

    protected abstract fun createAdapter(): RecyclerView.Adapter<*>

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
