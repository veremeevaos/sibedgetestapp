package com.mytrycatch.testapp.common.mvp

import android.content.Context


interface IView {
    fun showMessage(message: String)
    fun showMessage(messageResId: Int)
    fun context() : Context
}


interface IListView<M> : IView {
    fun setLoadingVisible(isVisible: Boolean)
    fun showData(dataset: List<M>)
    fun updateList()
    fun updateItem(position: Int)
    fun removeItem(item: M)
}


interface IPresenter<V : IView> {
    fun takeView(view: V)
    fun onViewAttached()
    fun dropView()
    fun onViewDetached()
}


interface IListPresenter<M, V : IListView<*>> : IPresenter<V> {
    fun onSwipeToRefresh()
    fun onItemClick(item: M) {}
}
