package com.mytrycatch.testapp.data.models

import org.simpleframework.xml.Element

class FeedItem {

    @field:Element(name = "id")
    var id: Long = 0

    @field:Element(name = "date")
    var date: String? = null

    @field:Element(name = "text")
    var text: String? = null

}