package com.mytrycatch.testapp.data.rest.datasources

import com.mytrycatch.testapp.data.models.Feed
import retrofit2.http.GET
import rx.Observable

interface FeedDataSource {

    @GET("testXmlFeed.xml")
    fun fetchFeed(): Observable<Feed>
}