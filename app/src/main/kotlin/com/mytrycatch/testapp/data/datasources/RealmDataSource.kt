package com.mytrycatch.testapp.data.datasources

import com.mytrycatch.testapp.data.models.Movie
import com.mytrycatch.testapp.data.realm.RealmMovie
import io.realm.Realm
import io.realm.RealmModel
import java.util.*

class RealmDataSource(val realm: Realm) : IRealmDataSource{

    override var movieList: List<Movie>?
        get() {
            val movies = ArrayList<Movie>()
            realm.where(RealmMovie::class.java).findAll().forEach { realmMovie -> movies.add(realmMovie.toMovieModel()) }
            return movies
        }

        set(value) {}

    override fun createOrUpdate(movie: Movie) {
        val realmMovie = RealmMovie()
        realmMovie.fromMovieModel(movie)
        if (realmMovie.id == 0L) {
            realmMovie.id = getId(RealmMovie::class.java)
        }

        realm.executeTransaction {
            realm.copyToRealmOrUpdate(realmMovie)
        }
    }

    override fun deleteMovie(movie: Movie) {
        val movieToDelete = realm.where(RealmMovie::class.java).equalTo("id", movie.id).findFirst()
        realm.executeTransaction {
            movieToDelete?.deleteFromRealm()
        }
    }

    private fun <T: RealmModel> getId( clazz: Class<T>): Long {
        var primaryKeyValue = realm.where(clazz).max("id")?.toLong() ?: 1L
        return ++primaryKeyValue
    }
}