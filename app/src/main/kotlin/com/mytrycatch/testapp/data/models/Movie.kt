package com.mytrycatch.testapp.data.models

import java.io.Serializable

class Movie(var name: String, var id: Long = 0,
        var isWatched: Boolean = false) : Serializable{
}