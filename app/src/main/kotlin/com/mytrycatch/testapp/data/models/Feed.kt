package com.mytrycatch.testapp.data.models

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root
import java.util.*

@Root(name = "result")
class Feed () {
    @field:Element  (name = "totalPages")
    var totalPages: Int? = 0
    @field:ElementList(entry = "quote", name = "quotes")
    var quotes: ArrayList<FeedItem>? = null
}