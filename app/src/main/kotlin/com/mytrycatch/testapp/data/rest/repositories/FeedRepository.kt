package com.mytrycatch.testapp.data.rest.repositories

import com.mytrycatch.testapp.data.models.Feed
import com.mytrycatch.testapp.data.rest.datasources.FeedDataSource
import rx.Observable
import javax.inject.Inject


class FeedRepository @Inject constructor(private val dataSource: FeedDataSource) : Repository() {

    fun fetchFeed(): Observable<Feed> {
        return dataSource.fetchFeed().compose(applySchedulers<Feed>())
    }
}