package com.mytrycatch.testapp.data.datasources

import com.mytrycatch.testapp.data.models.Movie

interface IRealmDataSource {
    var movieList: List<Movie>?

    fun createOrUpdate(movie: Movie)
    fun deleteMovie(movie: Movie)
}