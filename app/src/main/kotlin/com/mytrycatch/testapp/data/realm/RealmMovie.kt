package com.mytrycatch.testapp.data.realm

import com.mytrycatch.testapp.data.models.Movie
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class RealmMovie : RealmObject() {

    @PrimaryKey
    var id: Long = 0
    var name: String = ""
    var isWatched: Boolean = false

    fun toMovieModel() : Movie{
        return Movie(name, id, isWatched)
    }

    fun fromMovieModel(movie: Movie) {
        id = movie.id
        name = movie.name
        isWatched = movie.isWatched
    }
}