package com.mytrycatch.testapp.data.rest

import okhttp3.Interceptor
import okhttp3.Response
import java.net.UnknownHostException

class TestInterceptor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {
        try {
            val request = chain!!
                        .request()!!
                        .newBuilder()
                        .build()
            return chain.proceed(request)
        } catch (e: UnknownHostException) {
            throw Exception("No Internet connection")
        }
    }
}