package com.mytrycatch.testapp.modules.list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.*
import com.mytrycatch.testapp.Constants
import com.mytrycatch.testapp.R
import com.mytrycatch.testapp.common.mvp.BaseListFragment
import com.mytrycatch.testapp.data.models.Movie
import com.mytrycatch.testapp.modules.moviedetails.MovieDetailsActivity
import kotlinx.android.synthetic.main.layout_recycler_view.*

class WatchListFragment() : BaseListFragment<Movie, IWatchListView, IWatchListPresenter>(), IWatchListView, OnMovieClickListener, View.OnCreateContextMenuListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_list, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        activity.menuInflater.inflate(R.menu.menu_list_item, menu)
        super.onCreateContextMenu(menu, v, menuInfo)
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.listItemEditMenuItem -> presenter?.onEditSelectedItem()
            R.id.listItemDeleteMenuItem -> presenter?.onDeleteSelectedItem()
        }
        return super.onContextItemSelected(item)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.listAddMenuItem) {
            presenter?.onAddItemClick()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = WatchListPresenter(this)
        presenter?.onViewAttached()
    }

    override fun initViews() {
        super.initViews()
        registerForContextMenu(recyclerView)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val movie = data?.getSerializableExtra(Constants.KEY_MOVIE) as Movie
            if (requestCode == Constants.REQUEST_CODE_MOVIE_DETAILS_ADD) {
                dataset.add(movie)
                presenter?.onItemAdded(movie)
                updateList()
            } else if (requestCode == Constants.REQUEST_CODE_MOVIE_DETAILS_EDIT) {
                val position = data!!.getIntExtra(Constants.KEY_MOVIE_POSITION, -1)
                if (position != -1) {
                    dataset[position] = movie
                    presenter?.onItemEdited(movie)
                    updateItem(position)
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun createAdapter(): RecyclerView.Adapter<WatchListAdapter.MovieViewHolder> {
        adapter = WatchListAdapter(dataset, this)
        return adapter as WatchListAdapter
    }

    override fun navigateToMovieDetails(movie: Movie?, position: Int?) {
        val intent = Intent(activity, MovieDetailsActivity::class.java)
        var requestCode = Constants.REQUEST_CODE_MOVIE_DETAILS_ADD
        if (movie != null) {
            intent.putExtra(Constants.KEY_MOVIE, movie)
            intent.putExtra(Constants.KEY_MOVIE_POSITION, position)
            requestCode = Constants.REQUEST_CODE_MOVIE_DETAILS_EDIT
        }

        startActivityForResult(intent, requestCode)
    }

    override fun onClick(movie: Movie, position: Int) {
        presenter?.onEditItemClick(movie, position)
    }

    override fun onLongClick(movie: Movie, position: Int) {
        presenter?.onItemLongClick(movie, position)
    }

    override fun onCheckChangedClick(movie: Movie, position: Int) {
        presenter?.onCheckChangedClick(movie, position)
    }

}