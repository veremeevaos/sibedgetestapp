package com.mytrycatch.testapp.modules.main

import android.app.Fragment
import android.os.Bundle
import android.os.PersistableBundle
import android.support.annotation.IdRes
import android.support.annotation.StringRes
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import android.view.View
import com.mytrycatch.testapp.Constants
import com.mytrycatch.testapp.R
import com.mytrycatch.testapp.common.mvp.MvpActivity
import com.mytrycatch.testapp.modules.list.WatchListFragment
import com.mytrycatch.testapp.modules.map.MapFragment
import com.mytrycatch.testapp.modules.scaling.ScalingFragment
import com.mytrycatch.testapp.modules.service.FeedListFragment
import com.mytrycatch.testapp.modules.settings.SettingsFragment
import com.mytrycatch.testapp.modules.tabbar.DrawerPresenter
import com.mytrycatch.testapp.modules.tabbar.IDrawerPresenter
import com.mytrycatch.testapp.modules.tabbar.IDrawerView
import kotlinx.android.synthetic.main.activity_drawer.*

class DrawerActivity : MvpActivity<IDrawerView, IDrawerPresenter>(), IDrawerView  {

    enum class DrawerItem(@StringRes val titleId: Int, @IdRes val itemId: Int) {
        LIST(R.string.drawer_item_list_text, R.id.drawerItemList),
        SCALING(R.string.drawer_item_scaling_text, R.id.drawerItemScaling),
        SERVICE(R.string.drawer_item_service_text, R.id.drawerItemService),
        MAP(R.string.drawer_item_map_text, R.id.drawerItemMap),
        SETTINGS(R.string.drawer_item_settings_text, R.id.drawerItemSettings)
    }

    private var drawerToggle: ActionBarDrawerToggle? = null
    private var selectedDrawerItem: DrawerItem = DrawerItem.LIST
    private var isSelectedDrawerItemChanged = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawer)

        initViews()

        if (savedInstanceState != null) {
            selectedDrawerItem = DrawerItem.values()[savedInstanceState.getInt(Constants.KEY_SELECTED_DRAWER_ITEM, DrawerItem.LIST.ordinal)]
        }

        navigationView.setCheckedItem(selectedDrawerItem.itemId)
        supportActionBar?.title = getString(selectedDrawerItem.titleId)
        navigateTo(selectedDrawerItem)

        presenter = DrawerPresenter(this)
        presenter?.onViewAttached()
    }

    override fun onPostCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onPostCreate(savedInstanceState, persistentState)
        drawerToggle?.syncState()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (drawerToggle != null && drawerToggle!!.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putInt(Constants.KEY_SELECTED_DRAWER_ITEM, selectedDrawerItem.ordinal)
        super.onSaveInstanceState(outState)
    }

    private fun initViews() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp)
        initDrawer()
    }

    private fun initDrawer() {
        navigationView.setNavigationItemSelectedListener { menuItem ->
            val id = menuItem.itemId
            isSelectedDrawerItemChanged = id != selectedDrawerItem.itemId
            if (isSelectedDrawerItemChanged) {
                when (id) {
                    R.id.drawerItemList -> selectedDrawerItem = DrawerItem.LIST
                    R.id.drawerItemScaling -> selectedDrawerItem = DrawerItem.SCALING
                    R.id.drawerItemService -> selectedDrawerItem = DrawerItem.SERVICE
                    R.id.drawerItemMap -> selectedDrawerItem = DrawerItem.MAP
                    R.id.drawerItemSettings -> selectedDrawerItem = DrawerItem.SETTINGS
                }
                supportActionBar?.title = getString(selectedDrawerItem.titleId)
            }
            drawerLayout!!.closeDrawers()
            true
        }

        drawerToggle = object : ActionBarDrawerToggle(
                this,
                drawerLayout,
                R.string.drawer_open_text,
                R.string.drawer_close_text) {

            override fun onDrawerClosed(view: View) {
                super.onDrawerClosed(view)
                if (isSelectedDrawerItemChanged) {
                    navigateTo(selectedDrawerItem)
                }
            }

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
            }
        }

        drawerLayout!!.addDrawerListener(drawerToggle as ActionBarDrawerToggle)
        drawerToggle?.syncState()
    }

    private fun navigateTo(drawerItem: DrawerItem) {
        val fragment = when (drawerItem) {
            DrawerItem.LIST -> WatchListFragment()
            DrawerItem.MAP -> MapFragment()
            DrawerItem.SERVICE -> FeedListFragment()
            DrawerItem.SETTINGS -> SettingsFragment()
            DrawerItem.SCALING -> ScalingFragment()
        }

        isSelectedDrawerItemChanged = false

        setContentFragment(fragment)
    }

    private fun setContentFragment(fragment: Fragment) {
        fragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment, fragment.javaClass.simpleName)
                .commit()
    }
}
