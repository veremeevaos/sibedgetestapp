package com.mytrycatch.testapp.modules.scaling

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mytrycatch.testapp.Constants
import com.mytrycatch.testapp.R
import com.mytrycatch.testapp.common.mvp.MvpFragment
import kotlinx.android.synthetic.main.fragment_scaling.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File

class ScalingFragment() : MvpFragment<IScalingView, IScalingPresenter>(), IScalingView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, viewGroup: ViewGroup, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_scaling, viewGroup, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()

        presenter = ScalingPresenter(this)
        presenter?.onViewAttached()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, object : DefaultCallback() {
            override fun onImagePickerError(e: Exception, source: EasyImage.ImageSource, type: Int) {
                showMessage(e.message.toString())
            }

            override fun onImagePicked(imageFile: File, source: EasyImage.ImageSource, type: Int) {
                presenter?.onImageSelected(imageFile.absolutePath)
            }
        })
    }

    override fun navigateToImagePreviewScreen(imagePath: String) {
        startActivity<ImagePreviewActivity>(Constants.KEY_IMAGE_PATH to imagePath)
    }

    override fun choosePhotoFromGallery() {
        EasyImage.openGallery(this, 0)
    }

    override fun takePhoto() {
        EasyImage.openCamera(this, 0)
    }


    private fun initViews() {
        scalingTakePhotoImageButton.onClick { presenter?.onTakePhotoClick() }
        scalingPickFromGalleryImageButton.onClick { presenter?.onChoosePhotoFromGalleryClick() }
    }
}