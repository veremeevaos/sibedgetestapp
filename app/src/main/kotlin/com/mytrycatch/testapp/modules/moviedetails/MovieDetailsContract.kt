package com.mytrycatch.testapp.modules.moviedetails

import android.support.annotation.StringRes
import com.mytrycatch.testapp.common.mvp.IPresenter
import com.mytrycatch.testapp.common.mvp.IView
import com.mytrycatch.testapp.data.models.Movie

interface IMovieDetailsView: IView {
    fun navigateBack(movie: Movie? = null)
    fun showUnsavedChangesAlertDialog()
    fun showNameFieldError(@StringRes message: Int)
    fun fillForm(movie: Movie? = null)
}

interface IMovieDetailsPresenter: IPresenter<IMovieDetailsView>{
    fun onDoneClick()
    fun onRevertClick()
    fun onNameChanged(name: String?)
}