package com.mytrycatch.testapp.modules.scaling

import com.mytrycatch.testapp.common.mvp.IPresenter
import com.mytrycatch.testapp.common.mvp.IView

interface IScalingView : IView {
    fun navigateToImagePreviewScreen(imagePath: String)
    fun takePhoto()
    fun choosePhotoFromGallery()
}

interface IScalingPresenter : IPresenter<IScalingView> {
    fun onImageSelected(imagePath: String)
    fun onTakePhotoClick()
    fun onChoosePhotoFromGalleryClick()
}