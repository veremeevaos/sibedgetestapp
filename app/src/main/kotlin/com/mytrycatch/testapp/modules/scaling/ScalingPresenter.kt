package com.mytrycatch.testapp.modules.scaling

import android.Manifest
import android.os.Build
import com.mytrycatch.testapp.R
import com.mytrycatch.testapp.common.mvp.RxPresenter
import com.tbruyelle.rxpermissions.RxPermissions
import rx.lang.kotlin.plusAssign

class ScalingPresenter(view: IScalingView) : RxPresenter<IScalingView>(view), IScalingPresenter {

    override fun onViewAttached() {
    }

    override fun onImageSelected(imagePath: String) {
        view?.navigateToImagePreviewScreen(imagePath)
    }

    override fun onTakePhotoClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            subscriptions += RxPermissions.getInstance(view?.context())
                    .request(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE)
                    .subscribe({ granted ->
                        if (granted) {
                            view?.takePhoto()
                        } else {
                            view?.showMessage(R.string.scaling_error_need_camera_permission_text)
                        }
                    })
        } else {
            view?.takePhoto()
        }
    }

    override fun onChoosePhotoFromGalleryClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            subscriptions += RxPermissions.getInstance(view?.context())
                    .request(Manifest.permission.READ_EXTERNAL_STORAGE)
                    .subscribe({ granted ->
                        if (granted) {
                            view?.choosePhotoFromGallery()
                        } else {
                            view?.showMessage(R.string.scaling_error_need_storage_permission_text)
                        }
                    })
        } else {
            view?.choosePhotoFromGallery()
        }
    }

}