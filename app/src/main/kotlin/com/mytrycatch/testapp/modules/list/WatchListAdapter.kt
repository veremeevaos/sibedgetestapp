package com.mytrycatch.testapp.modules.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mytrycatch.testapp.R
import com.mytrycatch.testapp.data.models.Movie
import kotlinx.android.synthetic.main.item_movie.view.*

class WatchListAdapter(val movieList: List<Movie>, val itemClickListener: OnMovieClickListener? = null) :
        RecyclerView.Adapter<WatchListAdapter.MovieViewHolder>() {

    var recyclerView: RecyclerView? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        recyclerView = parent as RecyclerView
        return MovieViewHolder(view, itemClickListener)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(recyclerView, movieList[position], position)
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    class MovieViewHolder(view: View, val itemClickListener: OnMovieClickListener? = null) : RecyclerView.ViewHolder(view) {

        fun bind(recyclerView: RecyclerView?, item: Movie, position: Int) {
            itemView.apply {
                setOnClickListener { itemClickListener?.onClick(item, position) }
                setOnLongClickListener {
                    itemClickListener?.onLongClick(item, position)
                    recyclerView?.showContextMenuForChild(itemView)
                    true
                }

                itemMovieIsWatchedCheckBox.setOnCheckedChangeListener(null)
                itemMovieIsWatchedCheckBox.isChecked = item.isWatched
                itemMovieIsWatchedCheckBox.setOnCheckedChangeListener { compoundButton, b ->
                    run {
                        itemClickListener?.onCheckChangedClick(item, position)
                        if (item.isWatched) {
                            itemMovieImageView.setImageResource(R.drawable.ic_remove_red_eye_black_48dp)
                        } else {
                            itemMovieImageView.setImageResource(R.drawable.ic_camera_alt_black_48dp)
                        }
                    }
                }

                itemMovieNameTextView.text = item.name

                if (item.isWatched) {
                    itemMovieImageView.setImageResource(R.drawable.ic_remove_red_eye_black_48dp)
                } else {
                    itemMovieImageView.setImageResource(R.drawable.ic_camera_alt_black_48dp)
                }
            }
        }
    }
}