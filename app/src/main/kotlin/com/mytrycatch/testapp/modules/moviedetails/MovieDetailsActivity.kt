package com.mytrycatch.testapp.modules.moviedetails

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.annotation.StringRes
import android.text.Editable
import android.text.TextWatcher
import com.mytrycatch.testapp.Constants
import com.mytrycatch.testapp.R
import com.mytrycatch.testapp.common.mvp.MvpActivity
import com.mytrycatch.testapp.data.models.Movie
import kotlinx.android.synthetic.main.activity_movie_details.*
import org.jetbrains.anko.onClick

class MovieDetailsActivity : MvpActivity<IMovieDetailsView, IMovieDetailsPresenter>(), IMovieDetailsView  {

    enum class Mode(@StringRes val titleId: Int) {
        EDIT(R.string.movie_details_edit_text),
        ADD(R.string.movie_details_add_text)
    }

    var mode: Mode = Mode.ADD
    var position: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)

        val movie = intent.getSerializableExtra(Constants.KEY_MOVIE) as Movie?
        position = intent.getIntExtra(Constants.KEY_MOVIE_POSITION, -1)
        mode = if (movie == null) Mode.ADD else Mode.EDIT

        initViews()
        presenter = MovieDetailsPresenter(this, movie)
        presenter?.onViewAttached()
    }

    override fun onBackPressed() {
        presenter?.onRevertClick()
    }

    private fun initViews() {
        supportActionBar?.title = getString(mode.titleId)
        movieDetailsDoneButton.onClick { presenter?.onDoneClick() }
        movieDetailsRevertButton.onClick { presenter?.onRevertClick() }

        movieDetailsNameEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                movieDetailsNameTextInputLayout.error = null
                presenter?.onNameChanged(s.toString())
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    override fun navigateBack(movie: Movie?) {
        if (movie == null) {
            setResult(Activity.RESULT_CANCELED)
        } else {
            val data = Intent()
            data.putExtra(Constants.KEY_MOVIE, movie)
            data.putExtra(Constants.KEY_MOVIE_POSITION, position)
            setResult(Activity.RESULT_OK, data)
        }

        finish()
    }

    override fun showUnsavedChangesAlertDialog() {
        val dialog = AlertDialog.Builder(this)
                .setTitle(R.string.movie_details_dialog_title_back_to_list_text)
                .setMessage(R.string.movie_details_dialog_message_unsaved_changes_warning_text)
                .setPositiveButton(R.string.ok, {dialogInterface, arg -> navigateBack()} )
                .setNeutralButton(R.string.cancel, { dialogInterface, arg -> dialogInterface.dismiss() })
                .create()
        dialog.show()
    }

    override fun showNameFieldError(@StringRes message: Int) {
        movieDetailsNameTextInputLayout.error = getString(message)
    }

    override fun fillForm(movie: Movie?) {
        if (movie != null) {
            movieDetailsNameEditText.setText(movie.name)
        }
    }
}
