package com.mytrycatch.testapp.modules.map

import android.Manifest
import android.os.Build
import com.google.android.gms.location.LocationRequest
import com.mytrycatch.testapp.R
import com.mytrycatch.testapp.common.mvp.RxPresenter
import com.tbruyelle.rxpermissions.RxPermissions
import pl.charmas.android.reactivelocation.ReactiveLocationProvider
import rx.lang.kotlin.plusAssign

class MapPresenter(view: IMapView) : RxPresenter<IMapView>(view), IMapPresenter {

    val defaultLocationUpdatePeriodMillis = 5000L

    override fun onViewAttached() {
        fetchUserLocation()
    }

    private fun fetchUserLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            subscriptions += RxPermissions.getInstance(view?.context())
            .request(Manifest.permission.ACCESS_FINE_LOCATION)
                    .subscribe({ granted ->
                        if (granted) {
                            setupLocationProvider()
                        } else {
                            view?.showMessage(R.string.map_error_need_location_permission_text)
                        }
                    })
        } else {
            setupLocationProvider()
        }
    }

    private fun setupLocationProvider() {
        val request = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setFastestInterval(defaultLocationUpdatePeriodMillis)
                .setInterval(defaultLocationUpdatePeriodMillis)

        val locationProvider = ReactiveLocationProvider(view?.context())
        subscriptions += locationProvider.getUpdatedLocation(request)
                .subscribe(
                        { location ->
                            view?.showUserPosition(location.latitude, location.longitude)
                        },
                        { error ->
                            error.printStackTrace()
                        })
    }

}