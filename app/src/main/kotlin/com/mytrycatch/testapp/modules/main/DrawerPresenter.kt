package com.mytrycatch.testapp.modules.tabbar

import com.mytrycatch.testapp.common.mvp.RxPresenter

class DrawerPresenter(view: IDrawerView): RxPresenter<IDrawerView>(view), IDrawerPresenter {
}