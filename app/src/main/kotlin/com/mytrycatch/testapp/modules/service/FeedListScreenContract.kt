package com.mytrycatch.testapp.modules.service

import com.mytrycatch.testapp.common.mvp.IListPresenter
import com.mytrycatch.testapp.common.mvp.IListView
import com.mytrycatch.testapp.data.models.FeedItem

interface IFeedListView : IListView<FeedItem> {

}

interface IFeedListPresenter : IListPresenter<FeedItem, IFeedListView> {

}