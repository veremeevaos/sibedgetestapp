package com.mytrycatch.testapp.modules.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cocosw.bottomsheet.BottomSheet
import com.mytrycatch.testapp.R
import com.mytrycatch.testapp.common.LanguageUtils
import com.mytrycatch.testapp.common.mvp.MvpFragment
import kotlinx.android.synthetic.main.fragment_settings.*
import org.jetbrains.anko.onClick

class SettingsFragment() : MvpFragment<ISettingsView, ISettingsPresenter>(), ISettingsView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, viewGroup: ViewGroup, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, viewGroup, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()

        presenter = SettingsPresenter(this)
        presenter?.onViewAttached()
    }

    override fun showSelectedLanguage(language: LanguageUtils.Language) {
        settingsSelectedLanguageTextView.text = getString(language.titleId)
    }

    override fun updateUi() {
        activity.recreate()
    }

    private fun initViews() {
        settingsSelectLanguageView.onClick { showLanguagePicker() }
    }

    private fun showLanguagePicker() {
        BottomSheet.Builder(activity)
                .sheet(R.menu.menu_select_language)
                .listener { dialog, which ->
                    when (which) {
                        R.id.languageItemRussian -> presenter?.onLanguageSelected(LanguageUtils.Language.RU)
                        R.id.languageItemEnglish -> presenter?.onLanguageSelected(LanguageUtils.Language.EN)
                    }
                }.show()
    }
}