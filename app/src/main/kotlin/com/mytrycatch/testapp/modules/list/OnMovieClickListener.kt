package com.mytrycatch.testapp.modules.list

import com.mytrycatch.testapp.data.models.Movie

interface OnMovieClickListener {
    fun onClick(movie: Movie, position: Int)
    fun onLongClick(movie: Movie, position: Int)
    fun onCheckChangedClick(movie: Movie, position: Int)
}