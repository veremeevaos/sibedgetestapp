package com.mytrycatch.testapp.modules.settings

import com.mytrycatch.testapp.common.LanguageUtils
import com.mytrycatch.testapp.common.mvp.IPresenter
import com.mytrycatch.testapp.common.mvp.IView

interface ISettingsView : IView {
    fun showSelectedLanguage(language: LanguageUtils.Language)
    fun updateUi()
}

interface ISettingsPresenter : IPresenter<ISettingsView> {
    fun onLanguageSelected(language: LanguageUtils.Language)
}