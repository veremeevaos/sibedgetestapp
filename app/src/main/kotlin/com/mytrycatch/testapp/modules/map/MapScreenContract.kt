package com.mytrycatch.testapp.modules.map


import com.mytrycatch.testapp.common.mvp.IPresenter
import com.mytrycatch.testapp.common.mvp.IView

interface IMapView : IView {
    fun showUserPosition(latitude: Double, longitude: Double)
}

interface IMapPresenter : IPresenter<IMapView> {
}