package com.mytrycatch.testapp.modules.service

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import com.mytrycatch.testapp.common.mvp.BaseListFragment
import com.mytrycatch.testapp.data.models.FeedItem

class FeedListFragment() : BaseListFragment<FeedItem, IFeedListView, IFeedListPresenter>(), IFeedListView {

    var retainedFragment: FeedRetainedFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        retainedFragment = activity.fragmentManager.findFragmentByTag(FeedRetainedFragment::class.java.simpleName) as FeedRetainedFragment?

        if (retainedFragment == null) {
            retainedFragment = FeedRetainedFragment()
            activity.fragmentManager.beginTransaction().add(retainedFragment, FeedRetainedFragment::class.java.simpleName).commit()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (retainedFragment?.presenter == null) {
            presenter = FeedListPresenter(this)
            retainedFragment?.presenter = presenter as FeedListPresenter
            presenter?.onViewAttached()
        } else {
            presenter = retainedFragment?.presenter
            presenter?.takeView(this)
        }

    }

    override fun createAdapter(): RecyclerView.Adapter<FeedListAdapter.FeedViewHolder> {
        adapter = FeedListAdapter(dataset)
        return adapter as FeedListAdapter
    }

    override fun initViews() {
        super.initViews()
        swipeToRefreshEnabled = true
    }
}