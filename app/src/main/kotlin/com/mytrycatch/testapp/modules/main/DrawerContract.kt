package com.mytrycatch.testapp.modules.tabbar

import com.mytrycatch.testapp.common.mvp.IPresenter
import com.mytrycatch.testapp.common.mvp.IView

interface IDrawerView: IView {

}

interface IDrawerPresenter: IPresenter<IDrawerView>{

}