package com.mytrycatch.testapp.modules.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.mytrycatch.testapp.Constants
import com.mytrycatch.testapp.R
import com.mytrycatch.testapp.common.mvp.MvpFragment
import kotlinx.android.synthetic.main.fragment_map.*

class MapFragment() : MvpFragment<IMapView, IMapPresenter>(), IMapView, OnMapReadyCallback {

    var map: GoogleMap? = null
    var userMarker: Marker? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, viewGroup: ViewGroup, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_map, viewGroup, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()

        presenter = MapPresenter(this)
        presenter?.onViewAttached()
    }

    override fun showUserPosition(latitude: Double, longitude: Double) {
        mapLatitudeTextView.text = getString(R.string.map_latitude_formatted_text, latitude)
        mapLongitudeTextView.text = getString(R.string.map_longitude_formatted_text, longitude)
        mapPositionContainer.visibility = View.VISIBLE
        showMarkerOnMap(latitude, longitude)
    }

    override fun onMapReady(map: GoogleMap) {
        this.map = map
        map.moveCamera(CameraUpdateFactory.zoomTo(Constants.MAP_DEFAULT_ZOOM))
    }

    private fun showMarkerOnMap(latitude: Double, longitude: Double) {
        val userLocation = LatLng(latitude, longitude)
        if (userMarker == null) {
            userMarker = map?.addMarker(MarkerOptions().position(userLocation))
        } else {
            userMarker!!.position = userLocation
        }

        map?.moveCamera(CameraUpdateFactory.newLatLng(userLocation))
    }

    private fun initViews() {
        mapPositionContainer.visibility = View.INVISIBLE
        var mapFragment = this.childFragmentManager.findFragmentByTag(MapFragment::class.java.simpleName) as? MapFragment

        if (mapFragment == null) {
            mapFragment = MapFragment()
            this.childFragmentManager
                    .beginTransaction()
                    .add(R.id.mapContainer, mapFragment, MapFragment::class.java.simpleName)
                    .commit()
            this.childFragmentManager.executePendingTransactions()
        }

        mapFragment.getMapAsync(this)
    }
}