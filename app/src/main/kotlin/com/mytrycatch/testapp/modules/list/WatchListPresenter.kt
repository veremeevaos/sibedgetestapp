package com.mytrycatch.testapp.modules.list

import com.mytrycatch.testapp.common.mvp.BasePresenter
import com.mytrycatch.testapp.data.datasources.RealmDataSource
import com.mytrycatch.testapp.data.models.Movie
import io.realm.Realm

class WatchListPresenter(view: IWatchListView) : BasePresenter<IWatchListView>(view), IWatchListPresenter {

    val realmDataSource: RealmDataSource = RealmDataSource(Realm.getDefaultInstance())
    var lastSelectedItem: Movie? = null
    var lastSelectedItemPosition: Int? = null

    override fun onViewAttached() {
        fetchUsers()
    }

    override fun onSwipeToRefresh() {}

    override fun onAddItemClick() {
        view?.navigateToMovieDetails()
    }

    override fun onEditItemClick(movie: Movie, position: Int) {
        view?.navigateToMovieDetails(movie, position)
    }

    override fun onDeleteItemClick(movie: Movie, position: Int) {
        view?.removeItem(movie)
        realmDataSource.deleteMovie(movie)
    }

    override fun onCheckChangedClick(movie: Movie, position: Int) {
        movie.isWatched = !movie.isWatched
        realmDataSource.createOrUpdate(movie)
    }

    override fun onItemAdded(movie: Movie) {
        realmDataSource.createOrUpdate(movie)
    }

    override fun onItemEdited(movie: Movie) {
        realmDataSource.createOrUpdate(movie)
    }

    override fun onItemLongClick(movie: Movie, position: Int) {
        lastSelectedItem = movie
        lastSelectedItemPosition = position
    }

    override fun onDeleteSelectedItem() {
        if (lastSelectedItem == null || lastSelectedItemPosition == null) return

        onDeleteItemClick(lastSelectedItem!!, lastSelectedItemPosition!!)
    }

    override fun onEditSelectedItem() {
        if (lastSelectedItem == null || lastSelectedItemPosition == null) return

        onEditItemClick(lastSelectedItem!!, lastSelectedItemPosition!!)
    }

    private fun fetchUsers() {
        val movies = realmDataSource.movieList
        if (movies != null) {
            view?.showData(movies)
        }
    }

}