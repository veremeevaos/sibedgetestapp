package com.mytrycatch.testapp.modules.service

import com.mytrycatch.testapp.common.mvp.RxPresenter
import com.mytrycatch.testapp.data.models.FeedItem
import com.mytrycatch.testapp.data.rest.repositories.FeedRepository
import com.mytrycatch.testapp.di.DaggerNetworkComponent
import com.mytrycatch.testapp.di.modules.NetworkModule
import rx.Subscription

class FeedListPresenter(view: IFeedListView) : RxPresenter<IFeedListView>(view), IFeedListPresenter {

    lateinit var feedRepository: FeedRepository
    var dataSet: List<FeedItem>? = null
    var isRequestInProgress: Boolean = false
    var subscription: Subscription? = null

    override fun onSwipeToRefresh() {
        fetchFeed()
    }

    override fun onViewAttached() {
        feedRepository = DaggerNetworkComponent.builder()
                .networkModule(NetworkModule())
                .build()
                .provideFeedRepository()

        if (!isRequestInProgress && dataSet == null) {
            fetchFeed()
        } else if (!isRequestInProgress && dataSet != null) {
            view?.showData(dataSet!!)
        } else {
            view?.setLoadingVisible(isRequestInProgress)
        }
    }

    private fun fetchFeed() {
        isRequestInProgress = true
        view?.setLoadingVisible(true)
        subscription = feedRepository
                    .fetchFeed()
                    .subscribe(
                        { response ->
                            view?.setLoadingVisible(false)
                            isRequestInProgress = false
                            if (response.quotes != null) {
                                dataSet = response.quotes!!
                                view?.showData(dataSet!!)
                            }
                        },
                        { error ->
                            view?.setLoadingVisible(false)
                            isRequestInProgress = false
                            view?.showMessage(error.message.toString())
                            error.printStackTrace()
                        }
                    )
    }

    override fun onViewDetached() {
        super.onViewDetached()
        if (!isRequestInProgress) {
            subscription?.unsubscribe()
        }
    }

}