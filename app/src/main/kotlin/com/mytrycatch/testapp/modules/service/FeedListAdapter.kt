package com.mytrycatch.testapp.modules.service

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mytrycatch.testapp.R
import com.mytrycatch.testapp.data.models.FeedItem
import kotlinx.android.synthetic.main.item_feed.view.*

class FeedListAdapter(val feedList: List<FeedItem>) :
        RecyclerView.Adapter<FeedListAdapter.FeedViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_feed, parent, false)
        return FeedViewHolder(view)
    }

    override fun onBindViewHolder(holder: FeedViewHolder, position: Int) {
        holder.bind(feedList[position])
    }

    override fun getItemCount(): Int {
        return feedList.size
    }

    class FeedViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(item: FeedItem) {
            itemView.apply {
                itemFeedDateTextView.text = item.date
                itemFeedQuoteTextView.text = item.text
            }
        }
    }
}