package com.mytrycatch.testapp.modules.list

import com.mytrycatch.testapp.common.mvp.IListPresenter
import com.mytrycatch.testapp.common.mvp.IListView
import com.mytrycatch.testapp.data.models.Movie

interface IWatchListView : IListView<Movie> {
    fun navigateToMovieDetails(movie: Movie? = null, position: Int? = null)
}

interface IWatchListPresenter : IListPresenter<Movie, IWatchListView> {
    fun onAddItemClick()
    fun onItemAdded(movie: Movie)
    fun onItemLongClick(movie: Movie, position: Int)
    fun onItemEdited(movie: Movie)
    fun onEditItemClick(movie: Movie, position: Int)
    fun onDeleteItemClick(movie: Movie, position: Int)
    fun onEditSelectedItem()
    fun onDeleteSelectedItem()
    fun onCheckChangedClick(movie: Movie, position: Int)
}