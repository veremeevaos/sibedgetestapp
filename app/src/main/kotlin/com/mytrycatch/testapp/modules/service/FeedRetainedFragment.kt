package com.mytrycatch.testapp.modules.service

import android.app.Fragment
import android.os.Bundle

class FeedRetainedFragment: Fragment() {

    var presenter: FeedListPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }
}
