package com.mytrycatch.testapp.modules.moviedetails

import com.mytrycatch.testapp.R
import com.mytrycatch.testapp.common.mvp.RxPresenter
import com.mytrycatch.testapp.data.models.Movie

class MovieDetailsPresenter(view: IMovieDetailsView, var movie: Movie?): RxPresenter<IMovieDetailsView>(view), IMovieDetailsPresenter {

    var name: String? = null

    override fun onNameChanged(name: String?) {
        this.name = name
    }


    override fun onViewAttached() {
        super.onViewAttached()
        view?.fillForm(movie)
    }

    override fun onDoneClick() {
        if (validateName()) {
            if (movie == null) {
                movie = Movie(name!!)
            } else {
                movie!!.name = name!!
            }
            view?.navigateBack(movie)
        } else {
            view?.showNameFieldError(R.string.movie_details_error_empty_name_text)
        }
    }

    override fun onRevertClick() {
        if (movie?.name != name && name !=null && !name.isNullOrEmpty()) {
            view?.showUnsavedChangesAlertDialog()
        } else {
            view?.navigateBack()
        }
    }

    private fun validateName() : Boolean {
        return name != null && !name.isNullOrEmpty()
    }
}