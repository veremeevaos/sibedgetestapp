package com.mytrycatch.testapp.modules.settings

import com.mytrycatch.testapp.common.LanguageUtils
import com.mytrycatch.testapp.common.mvp.BasePresenter

class SettingsPresenter(view: ISettingsView) : BasePresenter<ISettingsView>(view), ISettingsPresenter {


    override fun onViewAttached() {
        view?.showSelectedLanguage(LanguageUtils.getLanguage(view!!.context()))
    }

    override fun onLanguageSelected(language: LanguageUtils.Language) {
        if (!LanguageUtils.getLanguage(view!!.context()).equals(language)) {
            LanguageUtils.setLocale(view!!.context(), language)
            view?.showSelectedLanguage(language)
            view?.updateUi()
        }
    }
}