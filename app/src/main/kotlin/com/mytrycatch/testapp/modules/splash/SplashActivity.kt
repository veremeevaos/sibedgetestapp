package com.mytrycatch.testapp.modules.splash

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import com.mytrycatch.testapp.Constants
import com.mytrycatch.testapp.R
import com.mytrycatch.testapp.modules.main.DrawerActivity
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.imageView
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.startActivity
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.lang.kotlin.subscribeWith
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        frameLayout {
            imageView {
                setImageResource(R.mipmap.ic_launcher)
                scaleType = ImageView.ScaleType.CENTER
            }.lparams {
                width = matchParent
                height = matchParent
            }
        }

        Observable.interval(Constants.SPLASH_SCREEN_DELAY_MILLIS, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .take(1)
                .subscribeWith {
                    onNext {
                        startActivity<DrawerActivity>()
                        finish()
                    }
                    onError {}
                }

    }

}


