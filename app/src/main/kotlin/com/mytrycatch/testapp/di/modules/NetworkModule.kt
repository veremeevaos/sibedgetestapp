package com.mytrycatch.testapp.di.modules;


import com.mytrycatch.testapp.Constants
import com.mytrycatch.testapp.data.rest.TestInterceptor
import com.mytrycatch.testapp.data.rest.datasources.FeedDataSource
import com.mytrycatch.testapp.data.rest.repositories.FeedRepository
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideDefaultHTTPClient(interceptor: TestInterceptor): OkHttpClient {
        val client = OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build()

        return client
    }

    @Provides
    @Singleton
    fun provideInterceptor(): TestInterceptor {
        return TestInterceptor()
    }

    @Provides
    @Singleton
    @Inject
    fun provideDefaultRetrofitAdapter(client: OkHttpClient): Retrofit {
        val adapter = Retrofit.Builder()
                .baseUrl(Constants.API_ENDPOINT_URL)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build()

        return adapter
    }

    @Provides
    @Singleton
    fun provideAuthDataSource(retrofit: Retrofit): FeedDataSource {
        return retrofit.create(FeedDataSource::class.java)
    }

    @Provides
    @Singleton
    @Inject
    fun provideFeedRepository(feedDataSource: FeedDataSource): FeedRepository {
        return FeedRepository(feedDataSource)
    }

}
