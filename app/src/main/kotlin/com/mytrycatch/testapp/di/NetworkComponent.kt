package com.mytrycatch.testapp.di

import com.mytrycatch.testapp.data.rest.repositories.FeedRepository
import com.mytrycatch.testapp.di.modules.NetworkModule
import dagger.Component
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(NetworkModule::class))
interface NetworkComponent {
    fun provideHttpClient(): OkHttpClient
    fun provideFeedRepository(): FeedRepository
}
