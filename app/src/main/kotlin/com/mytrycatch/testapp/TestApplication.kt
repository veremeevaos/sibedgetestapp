package com.mytrycatch.testapp

import android.app.Application
import android.content.res.Configuration
import com.mytrycatch.testapp.common.LanguageUtils
import io.realm.Realm


class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        LanguageUtils.onCreate(this)
        Realm.init(this)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LanguageUtils.updateConfig(this)
    }

}