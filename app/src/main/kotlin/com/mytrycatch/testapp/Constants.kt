package com.mytrycatch.testapp;

object Constants {
    const val KEY_IMAGE_PATH: String = "imagePath"
    const val KEY_LANGUAGE: String = "language"
    const val KEY_MOVIE: String = "movie"
    const val KEY_MOVIE_POSITION: String = "moviePosition"
    const val KEY_SELECTED_DRAWER_ITEM: String = "selectedDrawerItem"

    const val REQUEST_CODE_MOVIE_DETAILS_ADD: Int = 4856
    const val REQUEST_CODE_MOVIE_DETAILS_EDIT: Int = 4857

    const val MAP_DEFAULT_ZOOM: Float = 17.0f

    const val SPLASH_SCREEN_DELAY_MILLIS: Long = 1500L

    const val API_ENDPOINT_URL: String = "http://storage.space-o.ru/"

}
